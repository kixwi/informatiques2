/*
 * partie1.c
 *
 * Created: 23/03/2023 14:05:41
 * Author : louise5
 */ 

#include <avr/io.h>
#include "caviara.h"

void pause2s_timer1() {
	TCNT1 = 0;	//mise � 0 du compteur
	TCCR1A = 0;	//mise � 0 de la configuration timer 1 A
	TCCR1B = 0;	//mise � 0 de la configuration timer 1 B
	TCCR1B |= (1<<WGM12) | (1<<CS12) | (1<<CS10);	//mode CTC et divison de l'horloge syst�me par 1024
	OCR1A = 14000;	//comparaison � 7812, pour obtenir un cycle de comptage d'environ 1s
	while((TIFR & (1<<OCF1A))==0); //attente de compare match
	TIFR |= (1<<OCF1A);	//remise � 0 du flag de compare match
}

void SysInit() {
	lcdInit();
	lcdPrintf(0,0,"PORTE - K.L.");
	
}

void PCInit() {
	DDRC = 0;
	DDRC |= (1<<DDC6);
	PORTC = 0;	
}

void clavierInit() {
	DDRB = 0;
	PORTB = 0;
	DDRB |= ((1<<PB3) | (1<<PB2) | (1<<PB1));
	PORTB |= ((1<<PB7) | (1<<PB6) | (1<<PB5) | (1<<PB4)); //for�age du pull up
}

unsigned char clavier() {
	unsigned cref = 3;
	unsigned char inv;
	unsigned char tmp;
	char retour;
	for(int i=1; i<4; i++) {
		tmp = 0;
		tmp = 1<<i;
		inv = ~tmp;
		PORTB = inv;
		pause2s_timer1();
		for(int j=4; j<8; j++) {
			
		}
	}
	return retour;
}

int main(void)
{
	char inClavier;
	SysInit();
	PCInit();
	clavierInit();
    while (1) 
    {
		inClavier = clavier();
		lcdPrintf(16,0,"%c", inClavier);
    }
}

