// Caviar A driver lib
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

typedef unsigned char uchar;

void boardInit(void);

unsigned int adc(char ch);

void lcdInit(void);
void lcdPrintf(uchar p, uchar z, const char *fmt, ...);

void rtcread(char *b, char adr, char lng);
void rtcwrite(char *b, char adr, char lng);
void rtcdate(char *date);

void eeread(char *b, int a, int l);
void eewrite(char *b, int a, int l);
