#include <stdio.h>
#include <stdlib.h>

unsigned short int makeWord(unsigned char msb, unsigned char lsb) {
    unsigned short int i;
    i = msb<<8;
    i = i | lsb;
    return i;
}

int main()
{
    unsigned char msb = 0x12;
    unsigned char lsb = 0x34;

    unsigned short int i;
    i = makeWord(msb, lsb);
    printf("%x\n", i);
    return 0;
}
