#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i, cr;
    float f;
    char c;

    //saisie et affichage de I
    printf("Saisir i : ");
    printf("\n");
    cr = scanf("%d", &i);
    fflush(stdin);
    if(cr != 1) {
        printf("Erreur de saisie\n");
        exit(1);
    }
    else {
        printf("I vaut en decimal : %d = %04d\n",i,i); //affichage de i en format X et 00X (d�cimal)
        printf("I vaut en hexadecimal : %x = %04x\n",i,i); //affichage de i en format X et 00X (hexadecimal)
    }

    //saisie et affichage de f
    printf("Saisir f : ");
    printf("\n");
    cr = scanf("%f", &f);
    fflush(stdin);
    if(cr != 1) {
        printf("Erreur de saisie\n");
        exit(1);
    }
    else {
        printf("f vaut, avec deux chiffres decimaux : %.2f\n",f); //affichage de f avec deux chiffres apr�s la virgule
    }

    //saisie et affichage de c
    printf("Saisir c : ");
    printf("\n");
    cr = scanf("%c", &c);
    fflush(stdin);
    if(cr != 1) {
        printf("Erreur de saisie\n");
        exit(1);
    }
    else {
        printf("c vaut : %c = %d (decimal) = %x (hexa)\n",c,c,c); //affichage de c en caract�re, d�cimal et hexa
    }

    return 0;
}
