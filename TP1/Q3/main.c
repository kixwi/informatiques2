#include <stdio.h>
#include <stdlib.h>

int main()
{
    char c;
    char d;

    c = 0x3C;
    d = ~c & 0x33;
    printf("%02x\n", (unsigned char)d);

    c = 0x3C;
    d = c<<2;
    printf("%02x\n", (unsigned char)d);

    c = 0xC3;
    d = (c>>2) | 0x81;
    printf("%02x\n", (unsigned char)d);

    return 0;
}
