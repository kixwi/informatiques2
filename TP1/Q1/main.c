#include <stdio.h>
#include <stdlib.h>

int main()
{
    int i;
    i = 42;
    printf("I en format standard : %d \nEn format 4 caracteres : %04d\n", i,i);
    printf("I en hexadecimal : %x \nEn format 4 caracteres : %04x\n", i,i);

    float f;
    f = 3.1415927;
    printf("f vaut : %2.2f\n", f);

    char c = 'A';
    printf("c vaut : %c = %d (decimal) = %x (hexa)",c,c,c);

    return 0;
}
