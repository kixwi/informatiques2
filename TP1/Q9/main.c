#include <stdio.h>
#include <stdlib.h>
#include "iom32_iut.h"

void affiche_bin(unsigned char c) {
    unsigned char tmp;
    for(int i=7; i>=0; i=i-1) {
        if((c & (1<<i)) == 0) {
            printf("0");
        }
        else {
            printf("1");
        }
    }
}

int main()
{
    unsigned char c = 0xF0;
    affiche_bin(c);
    return 0;
}
