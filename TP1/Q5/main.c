#include <stdio.h>
#include <stdlib.h>

int main()
{
    unsigned char uc;
    unsigned char ud;

    uc = 0xC3;
    ud = uc>>4;
    printf("%02x\n", (unsigned char)ud);

    uc = 0xC3;
    ud = ~uc & 0xF0;
    printf("%02x\n", (unsigned char)ud);

    uc = 0x33;
    ud = uc | (1<<7);
    printf("%02x\n", (unsigned char)ud);
    return 0;
}
