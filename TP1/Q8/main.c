#include <stdio.h>
#include <stdlib.h>
#include "iom32_iut.h"

int main()
{
    UCSRB = ((1<<RXEN) | (1<<TXEN));
    printf("UCSRB : %02x\n", UCSRB);

    UCSRB = ((1<<RXEN) | (1<<TXEN));
    UCSRB |= (1<<RXCIE);
    printf("UCSRB : %02x\n", UCSRB);

    UCSRB = ((1<<RXEN) | (1<<TXEN));
    UCSRB &= (~(1<<RXEN));
    printf("UCSRB : %02x\n", UCSRB);

    UCSRB = 0x00; //remise � 0 du registre

    UCSRB |= ((1<<RXCIE)|(1<<TXCIE));
    printf("UCSRB : %02x\n", UCSRB);

    UCSRB &= (~((1<<RXCIE)|(1<<TXCIE)));
    printf("UCSRB : %02x\n", UCSRB);

    UCSRB |= (1<<RXEN);

    if((UCSRB & (1<<RXEN))!=0) {
        printf("test reussi !\n");
        printf("UCSRB : %02x\n", UCSRB);
        UCSRB &= ~(1<<RXEN);
        printf("UCSRB : %02x\n", UCSRB);
    }

    UCSRA = 0x00;

    if((UCSRA & (1<<RXC))==0) {
        printf("RXC = 0 \n");
    }


    return 0;
}
