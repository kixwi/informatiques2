#include <stdio.h>
#include <stdlib.h>

unsigned char msb(unsigned short int i) {
    unsigned char octet;
    octet = i>>8;
    return octet;
}

unsigned char lsb(unsigned short int i) {
    unsigned char octet;
    octet = i & 0xFF;
    return octet;
}

int main()
{
    unsigned char msbC;
    unsigned char lsbC;

    unsigned short int i;
    i=0x1234;

    msbC = msb(i);
    printf("%x\n",msbC);

    lsbC = lsb(i);
    printf("%x\n",lsbC);

    return 0;
}
