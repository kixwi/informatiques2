#include <stdio.h>
#include <stdlib.h>

unsigned char msb(unsigned short int i) {
    unsigned char bit;
    if((i & (1<<15))==0) {
        bit = 0;
    }
    else {
        bit = 1;
    }
    return bit;
}

unsigned char lsb(unsigned short int i) {
    unsigned char bit;
    if((i & (1<<0))==0) {
        bit = 0;
    }
    else {
        bit = 1;
    }
    return bit;
}

int main()
{
    unsigned char msbC;
    unsigned char lsbC;

    unsigned short int i;
    i=0x8232;

    msbC = msb(i);
    printf("%x\n",msbC);

    lsbC = lsb(i);
    printf("%x\n",lsbC);

    return 0;
}
