/*
 *------------------------------------------------------------------------------
 * IUT Limousin
 * Dept GEII Brive
 * TP info embarqu�e DUT1
 * OH 01/2017
 * Fonctions utilitaires
 *------------------------------------------------------------------------------
 */

#include <avr/io.h>

#ifndef UTIL_GEII_H
#define UTIL_GEII_H

/* Prototypes	*/
void init_porte() ;				// Commandes de la porte
void ouverture_porte() ;
void fermeture_porte() ;

void pause(int n) ;				// Attente de n secondes

#endif

