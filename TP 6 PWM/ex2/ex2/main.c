/*
 * ex2.c
 *
 * Created: 28/04/2023 16:20:48
 * Author : louise5
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

void pause2s() {
	TCCR1B |= (1<<WGM12) | (1<<CS12) | (1<<CS10);
	TIMSK1 |= (1<<OCIE1A);
	OCR1AH = 31249>>8;
	OCR1AL = 31249;
	sei();
}

int main(void)
{
    /* Replace with your application code */
    while (1) 
    {
		
    }
}

ISR(T)