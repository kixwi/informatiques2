/*
 * USART.h
 *
 * Created: 28/04/2023 09:33:47
 *  Author: louise5
 */ 


#ifndef USART_H_
#define USART_H_

void USART_Init(unsigned int baudrate);
unsigned char USART_Receive();
void USART_Transmit(unsigned char data);

#endif /* USART_H_ */