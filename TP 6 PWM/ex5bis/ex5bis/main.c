/*
 * ex5bis.c
 *
 * Created: 28/04/2023 14:57:08
 * Author : louise5
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include "USART.h"

void Sys_Init() {
	DDRD |= (1<<PIND6);
	sei();
}

void PWM() {
	TCCR0A |= (1<<COM0A1) | (1<<WGM01) | (1<<WGM00);
	TCCR0B |= (1<<CS02);
	
}

int main(void)
{
	Sys_Init();
	USART_Init(51);
	USART_Transmit('T');
    /* Replace with your application code */
    while (1) 
    {
		PWM();
    }
}

ISR(USART_RX_vect) {
	unsigned char tmp = UDR0;
	USART_Transmit(tmp);
	OCR0A = 26*(tmp - '0');
}
