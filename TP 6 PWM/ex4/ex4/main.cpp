/*
 * ex4.cpp
 *
 * Created: 28/04/2023 14:14:23
 * Author : louise5
 */ 

#include <avr/io.h>

void PWM25() {
	TCCR0A |= (1<<COM0A1) | (1<<WGM01) | (1<<WGM00);
	TCCR0B |= (1<<CS02);
	TCNT0 = 0;
	OCR0A = 64;
}

void Sys_Init() {
	DDRD |= (1<<PIND6);
}

int main(void)
{
	Sys_Init();
    while (1) 
    {
		PWM25();
    }
}

