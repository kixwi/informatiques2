/*
 *------------------------------------------------------------------------------
 * IUT Limousin
 * Dept GEII Brive
 * TP info embarqu�e DUT1
 * OH 01/2017
 * Fonctions de gestion du clavier port B
 *------------------------------------------------------------------------------
 */

#include <avr/io.h>

#ifndef CLAVIER_GEII_H
#define CLAVIER_GEII_H

/* Prototypes	*/
void init_clavier() ;			// init port B pour clavier
char clavier() ;				// lecture de la touche du clavier

#endif

