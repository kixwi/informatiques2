/*
 * main.c
 *
 * Created: 08/03/2023 09:26:15
 * Author : louise5
 */ 

#include <avr/io.h>
#include "caviara.h"

void system_init() {
	DDRB = 0x01;	//PORTB0 en sortie (LED0)
	PORTB = 0xFF;	//extinction de toutes les LEDs
	lcdInit();
	lcdPrintf(0,0,"EX 2 - K.L.");
}

void pause1s() {
	for(int i=0; i<100; i++) {
		TCNT0 = 0x00;	//mise � 0 du compteur
		TCCR0 = 0x00;	//mise � 0 de la configuration timer
		TCCR0 |= (1<<WGM01) | (1<<CS02) | (1<<CS00);	//mode CTC et divison de l'horloge syst�me par 1024
		OCR0 = 77;	//comparaison � 77, pour obtenir un cycle de comptage d'environ 10ms
		while((TIFR & (1<<OCF0))==0); //attente de compare match
		TIFR |= (1<<OCF0);	//remise � 0 du flag de compare match
	}
}


int main(void)
{
    system_init();
	
    while (1) 
    {
		PORTB &= ~(1<<PB0);	//mise � 0 de PORTB0 -> allumage LED0
		pause1s();
		PORTB |= (1<<PB0);
		pause1s();
    }
}
