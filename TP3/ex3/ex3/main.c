/*
 * main.c
 *
 * Created: 08/03/2023 09:26:15
 * Author : louise5
 */ 

#include <avr/io.h>
#include "caviara.h"

void system_init() {
	DDRB = 0x01;	//PORTB0 en sortie (LED0)
	PORTB = 0xFF;	//extinction de toutes les LEDs
	lcdInit();
	lcdPrintf(0,0,"EX 3 - K.L.");
}

void pause1s_timer1() {
		TCNT1 = 0;	//mise � 0 du compteur
		TCCR1A = 0;	//mise � 0 de la configuration timer 1 A
		TCCR1B = 0;	//mise � 0 de la configuration timer 1 B
		TCCR1B |= (1<<WGM12) | (1<<CS12) | (1<<CS10);	//mode CTC et divison de l'horloge syst�me par 1024
		OCR1A = 7812;	//comparaison � 7812, pour obtenir un cycle de comptage d'environ 1s
		while((TIFR & (1<<OCF1A))==0); //attente de compare match
		TIFR |= (1<<OCF1A);	//remise � 0 du flag de compare match
}

int main(void)
{
    system_init();
	
    while (1) 
    {
		PORTB &= ~(1<<PB0);	//mise � 0 de PORTB0 -> allumage LED0
		lcdPrintf(16, 0, "LED ON ");
		pause1s_timer1();
		PORTB |= (1<<PB0);
		lcdPrintf(16, 0, "LED OFF");
		pause1s_timer1();
    }
}
