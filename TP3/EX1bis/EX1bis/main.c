/*
 * EXERCICE1.c
 *
 * Created: 08/03/2023 09:26:15
 * Author : louise5
 */ 

#include <avr/io.h>

void system_init() {
	DDRB = 0x01;	//PORTB0 en sortie (LED0)
	PORTB = 0xFF;	//extinction de toutes les LEDs
}

void pause() {
	TCNT0 = 0x00;	//mise � 0 du compteur
	TCCR0 = 0x00;	//mise � 0 de la configuration timer
	TCCR0 |= (1<<CS02) | (1<<CS00);	//divison de l'horloge syst�me par 1024
	while((TIFR & (1<<TOV0))==0); //attente de l'overflow
	TIFR |= (1<<TOV0);	//remise � 0 du flag d'overflow
}


int main(void)
{
    system_init();
	
    while (1) 
    {
		PORTB &= ~(1<<PB0);	//mise � 0 de PORTB0 -> allumage LED0
		pause();
		PORTB |= (1<<PB0);
		pause();
    }
}

