/*
 * MCC.c
 *
 * Created: 01/03/2023 11:25:07
 * Author : louise5
 */ 
#include "caviara.h"
#include <avr/io.h>

int mot;

void sysInit(void) {
	DDRB = 0x00;
	DDRC = 0x3F;
	lcdInit();
	lcdPrintf(0,0,"Ex MCC - K.LOUISE");
	lcdPrintf(16,0, "ETAT MOT : ");
	lcdPrintf(26,0, "OFF ");
	mot = 0;
}

void Moteur_stop() {
	PORTC &= ~(1<<PC2);
	//PORTC &= ~((1<<PC0) | (1<<PC1));
	lcdPrintf(26,0, "OFF ");
	mot = 0;
}

void Moteur_gauche() {
	PORTC = (1<<PC2);
	PORTC |= ((1<<PC0) & ~(1<<PC1));
	lcdPrintf(26,0, "ON G");
	mot = 1;
}

void Moteur_droite() {
	PORTC = (1<<PC2);
	PORTC |= ((1<<PC1) & ~(1<<PC0));
	lcdPrintf(26,0, "ON D");
	mot = 2;
}

int main(void)
{
	sysInit(); 
	
    while (1) 
    {
		//appui sur SB7
		if((PINB & (1<<PB7))==0) {
			Moteur_stop();
		}
		//appui sur SB0 -> moteur tourne � gauche
		if(((PINB & (1<<PB0))==0)) {
			//moteur � l'arret
			if(mot==0) {
				Moteur_gauche();
			}
			//moteur en rotation � droite
			/*if(mot==2) {
				Moteur_stop();
				Moteur_gauche();
			}*/
		}
		//appui sur SB1 -> moteur tourne � droite
		if(((PINB & (1<<PB1))==0)) {
			//moteur � l'arret
			if(mot==0) {
				Moteur_droite();
			}
			//moteur en rotation � gauche
			/*if(mot==1) {
				Moteur_stop();
				Moteur_droite();
			}*/
		}
    }
}

