/*
 * MANIP1.c
 *
 * Created: 01/03/2023 10:23:27
 * Author : louise5
 */ 
#include "caviara.h"
#include <avr/io.h>

//init
void system_init(void) {
	DDRB = 0xF0;	//Portb sortie (de 7 � 4) et entr�e (3 � 0)
	PORTB = 0xF0;	//extinction des LEDS par d�faut
	lcdInit();
}

//Prog principal
int main(void)
{
	system_init();
   
    while (1) 
    {
		//si SBO appuy�, LED 4 allum�e
		if((PINB & (1<<PB0))==0) {
			PORTB &= ~(1<<PB4);
			lcdPrintf(0, 0, "LED 4 ON ");
		}
		
		//si SB1 appuy�, LED 7 allum�e
		if((PINB & (1<<PB1))==0) {
			PORTB &= ~(1<<PB7);
			lcdPrintf(16, 0, "LED 7 ON ");
		}	
		
		//si SB2 appuy�, LED 4 �teinte
		if((PINB & (1<<PB2))==0) {
			PORTB |= (1<<PB4);
			lcdPrintf(0, 0, "LED 4 OFF");
		}
		
		//si SB3 appuy�, LED 7 �teinte
		if((PINB & (1<<PB3))==0) {
			PORTB |= (1<<PB7);
			lcdPrintf(16, 0, "LED 7 OFF");
		}
    }
}

