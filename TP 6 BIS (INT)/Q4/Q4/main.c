/*
 * Q4.c
 *
 * Created: 30/03/2023 16:28:37
 * Author : louise5
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

static int cpt = 0;
int dLong = 31250;
int dCourt = 7812;
int delai;
int sos;

void USART_Init(unsigned int baudrate);
unsigned char USART_Receive();
void USART_Transmit(unsigned char data);
void chrono(int delai);
void pause2s();
void Sys_Init();

int main(void)
{
    Sys_Init();
	USART_Init(51);
	delai = dLong;
	
    while (1) 
    {
		chrono(delai);
	}
	
}

// Initialize UART
void USART_Init( unsigned int baudrate )
{
	// Set the baud rate
	UBRR0H = (unsigned char) (baudrate>>8);
	UBRR0L = (unsigned char) baudrate;
	
	//RAZ du registre de controle A
	UCSR0A = 0x00 ;

	// Enable UART receiver and transmitter
	UCSR0B = ( ( 1 << RXEN0 ) | ( 1 << TXEN0 ) );
	
	// Set frame format: 8 data 2stop
	UCSR0C = (1<<USBS0)|(1<<UCSZ01)|(1<<UCSZ00);
	
	//interruptions
	UCSR0B |= (1<<RXCIE0) | (1<<TXCIE0);
	
	sei();
}


// Read and write functions
unsigned char USART_Receive( void )
{
	//Wait for incoming data
	while ( !(UCSR0A & (1<<RXC0)) );
	
	//Return the data
	return UDR0;
}

void USART_Transmit( unsigned char data )
{
	//Wait for empty transmit buffer
	while ( !(UCSR0A & (1<<UDRE0)) )
	;
	//Start transmission
	UDR0 = data;
}

void chrono(int delay) {
	TCCR1A = 0x00;
	TCCR1B |= (1<<WGM12) | (1<<CS12) | (1<<CS10);
	OCR1AH = ((delay+6250)>>8);
	OCR1AL = (delay+6250);
	OCR1BH = (delay>>8);
	OCR1BL = delay;
	TIMSK1 |= (1<<OCIE1A) | (1<<OCIE1B);
	sei();
}

void pause2s() {
	for(int i=0; i<151; i++) {
		TCCR0A |= (1<<WGM01);
		TCCR0B |= (1<<CS02) | (1<<CS00);
		OCR0A = 208;
		while((TIFR0 & (1<<OCF0A))==0);
		TIFR0 |= (1<<OCF0A);
	}
}

void Sys_Init() {
	DDRD |= (1<<PD4);
	PORTD = 0x00;
	sei();
}

ISR(USART_RX_vect) {
	unsigned char tmp = USART_Receive();
	USART_Transmit(tmp);
	if(tmp==0x44) {
		USART_Transmit('1');
	}
	else if(tmp==0x53) {
		USART_Transmit('0');
	}
}

ISR(TIMER1_COMPA_vect) {
	//cli();
	PORTD |= (1<<PD4);
	
	if(cpt==3) {
		delai = dCourt;
	}
	else if(cpt==6) {
		delai = dLong;
	}
	else if(cpt==9) {
		PORTD &= ~(1<<PD4);
		pause2s();
		PORTD |= (1<<PD4);
		cpt = 0;
	}
	cpt = cpt +1;
	//sei();
}

ISR(TIMER1_COMPB_vect) {
	//cli();
	PORTD &= ~(1<<PD4);
	//sei();
}
