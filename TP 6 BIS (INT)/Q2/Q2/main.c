/*
 * Q2.c
 *
 * Created: 30/03/2023 15:35:07
 * Author : louise5
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

static int cpt = 0;

void Sys_Init() {
	DDRD = 0x00;
	DDRD |= (1<<PD4);
	PORTD = 0x00;
}

void chrono() {
	TCCR0A = 0x00;
	TCCR0B |= (1<<CS02) | (1<<CS00);
	TIMSK0 |= (1<<TOIE0);
	sei();
}

int main(void)
{
	Sys_Init();
	chrono();
    while (1) 
    {
    }
}

ISR(TIMER0_OVF_vect){
	cli();
	cpt = cpt +1;
	if(cpt>10) {
		PORTD = ~PORTD;
		cpt = 0;
	}
	sei();
}