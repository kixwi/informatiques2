/*
 * Q1.c
 *
 * Created: 30/03/2023 15:18:06
 * Author : louise5
 */ 

#include <avr/io.h>

void Sys_Init() {
	DDRD = 0x00;
	DDRB |= (1<<PD7) | (1<<PD6) | (1<<PD5) | (1<<PD4);
	PORTD = 0x00;
}

int main(void)
{
    Sys_Init();
    while (1) 
    {
		if((PIND & (1<<PIND2))!=0) {
			PORTD &= ~((1<<PD6)|(1<<PD7));
			PORTD |= (1<<PD4) | (1<<PD5);
		}
		else if((PIND & (1<<PIND3))!=0) {
			PORTD &= ~((1<<PD4)|(1<<PD5));
			PORTD |= (1<<PD6) | (1<<PD7);
		}
    }
}

