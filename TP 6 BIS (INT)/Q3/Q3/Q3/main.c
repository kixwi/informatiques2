/*
 * Q3.c
 *
 * Created: 30/03/2023 16:12:13
 * Author : louise5
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>

void Sys_Init() {
	DDRD = 0x00;
	DDRD |= (1<<PD4);
	PORTD = 0x00;
	EIMSK |= (1<<INT0);	//validation de l'interruption externe sur INT0
	EICRA |= (1<<ISC01) | (1<<ISC00); //trigger when rising edge on INT0
	sei();
}

int main(void)
{
    Sys_Init();
    while (1) 
    {
    }
}

ISR(INT0_vect) {
	PORTD = ~PORTD;
}