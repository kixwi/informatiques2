/*
 * Ex1.c
 *
 * Created: 15/03/2023 14:29:12
 * Author : louise5
 */ 

#include <avr/io.h>
#include "caviara.h"
#include "USART.h"

void SysInit() {
	lcdInit();
	lcdPrintf(0,0,"EX 1 - K.L.");
}

int main(void)
{
	SysInit();
    USART_Init(25);
	unsigned char data;
    while (1) 
    {
		data = USART_Receive();
		lcdPrintf(16,0, "%c", data);
		USART_Transmit(data);
    }
}

