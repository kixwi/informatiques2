/*
 * Ex1.c
 *
 * Created: 15/03/2023 14:29:12
 * Author : louise5
 */ 

#include <avr/io.h>

void USART_SetBaudrate(int val) {
	UBRRH &= ~(1<<URSEL);
	UBRRL = val;
}

void USART_Init(int val) {
	USART_SetBaudrate(val);
	UCSRC |= (1<<URSEL);
	UCSRA &= ~((1<<U2X) | (1<<MPCM));
	UCSRB |= (1<<RXEN) | (1<<TXEN);
	UCSRC |= (1<<URSEL) | (1<<UPM1) | (1<<USBS) | (1<<UCSZ1) | (1<<UCSZ0);
} 

int main(void)
{
    USART_Init(25);
    while (1) 
    {
    }
}

