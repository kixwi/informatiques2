#ifndef USART_H
#define USART_H

void USART_Init(int val);
void USART_SetBaudrate(int val);
unsigned char USART_Receive();
void USART_Transmit(unsigned char data);

#endif