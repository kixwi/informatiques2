/*
 * ex4.c
 *
 * Created: 15/03/2023 16:10:57
 * Author : louise5
 */ 

#include <avr/io.h>
#include "caviara.h"
#include "USART.h"

void SysInit() {
	lcdInit();
	lcdPrintf(0,0,"EX 4 - K.L.");
}

void USART_Tx_String(char s[]) {
	for(int i=0; s[i]!='\0'; i++) {
		USART_Transmit(s[i]);
		lcdPrintf(i+16,0,"%c", s[i]);
	}
}

int main(void)
{
	SysInit();
	USART_Init(25);
    char s[20] = "Obi-wan Kenobi";
	USART_Tx_String(s);
    while (1) 
    {
		
    }
}

