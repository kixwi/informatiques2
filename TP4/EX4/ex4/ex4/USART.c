#include "USART.h"
#include <avr/io.h>

void USART_SetBaudrate(int val) {
	UBRRH = (unsigned char)(val>>8);
	UBRRL = (unsigned char)val;
}

void USART_Init(int val) {
	USART_SetBaudrate(val);
	UCSRC = (1<<URSEL);
	UCSRA = 0;
	UCSRB |= (1<<RXEN) | (1<<TXEN);
	UCSRC |= (1<<URSEL) | (1<<UPM1) | (1<<USBS) | (1<<UCSZ1) | (1<<UCSZ0);
} 

unsigned char USART_Receive(){
	//attente d'une donn�e
	while ( !(UCSRA & (1<<RXC)) );
	//retour de la donn�e
	return UDR;
}

void USART_Transmit( unsigned char data ){
	//attente du buffer vide
	while ( !( UCSRA & (1<<UDRE)) );
	//�criture de la donn�e � transmettre
	UDR = data;
}