// AVR306: Using the AVR UART in C
// Routines for polled USART
// Last modified: 02-06-21
// Modified by: AR
// Modifie par OH pour test Arduino UNO - IUT GEII 08/2011

// Includes
#include <avr/io.h>
#include <avr/interrupt.h>

// Prototypes
void USART_Init(unsigned int baudrate);
unsigned char USART_Receive(void);
void USART_Transmit(unsigned char data);
void Sys_Init();

//Main - a simple test program
void main()
{
	//Set the baudrate to 19,200 bps using a 16.000MHz crystal : 51 
	USART_Init(51);

	Sys_Init();

	while(1)
	{
		//vide, gestion de l'echo par l'interruption
	}
}

// Initialize UART
void USART_Init( unsigned int baudrate )
{
	// Set the baud rate
	UBRR0H = (unsigned char) (baudrate>>8);
	UBRR0L = (unsigned char) baudrate;
	
	//RAZ du registre de controle A
	UCSR0A = 0x00 ;

	// Enable UART receiver and transmitter 
	UCSR0B = ( ( 1 << RXEN0 ) | ( 1 << TXEN0 ) );
	
	// Set frame format: 8 data 2stop
	UCSR0C = (1<<USBS0)|(1<<UCSZ01)|(1<<UCSZ00);
	
	//interruptions 
	UCSR0B |= (1<<RXCIE0) | (1<<TXCIE0);
	
	sei();
}


// Read and write functions
unsigned char USART_Receive( void )
{
	//Wait for incoming data
	while ( !(UCSR0A & (1<<RXC0)) );
	
	//Return the data
	return UDR0;
}

void USART_Transmit( unsigned char data )
{
	//Wait for empty transmit buffer
	while ( !(UCSR0A & (1<<UDRE0)) )
	;
	//Start transmission
	UDR0 = data;
}

void Sys_Init() {
	DDRB = 0x00;
	DDRB |= (1<<PORTB5);
	PORTB = 0x00;
}

//gestion de l'interruption
ISR(USART_RX_vect) {
	cli();
	//echo
	USART_Transmit( USART_Receive() );
	sei();
}

ISR(USART_TX_vect) {
	cli();
	PORTB = ~(PORTB);
	sei();
}